require 'commander/import'
require 'pg'

program :name, 'Eligibility Log Dummy Member Flag Backfill'
program :version, '1.0.0'
program :description, 'backfills missing value of using_dummy_member column for all eligibility logs in the Protoss database.'
always_trace!

# Database config
@db_user = ENV['DB_USER']
@db_pass = ENV['DB_PASS']
@db_host = ENV['DB_HOST']
@db_name = ENV['DB_NAME']
@db_port = ENV['DB_PORT']
# Database config

command :backfill do |c|
  c.syntax      = 'backfill'
  c.description = 'backfills missing value of using_dummy_member column for all eligibility logs in the Protoss database.'
  c.option '--dry-run', 'Dry run will not execute SQL statements but print them to Std out instead.'
  c.option '--show-progress', 'Shows total count of members updated.'
  c.option '--size Integer', Integer, 'batch size of the backfilling'
  c.action do |args, options|
    options.default dry_run: false, show_progress: true, size: 1000
    say 'Backfilling...'
    @dry_run       = options.dry_run
    @show_progress = options.show_progress
    @size          = options.size
    backfill
  end
end

def backfill # Set up database connection
  if @db_user && @db_pass && @db_host && @db_name && @db_port
    conn = PG.connect(:dbname => @db_name, :host => @db_host, :port => @db_port, :user => @db_user, :password => @db_pass)
  else
    conn = PGconn.open(:dbname => 'protoss_api_development')
  end

  # Build dummy member hash
  dummys = {}
  conn.exec('
    SELECT card_id, first_name, last_name, gender, date_of_birth, group_number
    FROM (
          SELECT DISTINCT dummy_member_id
          FROM group_numbers
          WHERE dummy_member_id IS NOT NULL
    ) dummy_ids
    JOIN members ON dummy_ids.dummy_member_id = members.id
    JOIN membership_cards ON members.membership_card_id = membership_cards.id
    JOIN people ON members.person_id = people.id
  ').to_a.each do |dummy|
    dummys["#{dummy['card_id']}#{dummy['first_name']}#{dummy['last_name']}#{dummy['date_of_birth']}#{dummy['gender']}"] = true
  end

  logs = conn.exec('
    SELECT
      id,
      eligibility_card_card_id card_id,
      eligibility_record_first_name first_name,
      eligibility_record_last_name last_name,
      eligibility_record_date_of_birth date_of_birth,
      eligibility_record_gender :: INT gender
    FROM eligibility_logs
    WHERE using_dummy_member IS NULL
  ').to_a

  count = 0
  total = logs.length

  while count < total
    # reset for batch
    update_ids = []

    # go thru currect batch
    cap        = [count + @size, total].min
    for i in count..(cap - 1)
      log = logs[i]
      if dummys["#{log['card_id']}#{log['first_name']}#{log['last_name']}#{log['date_of_birth']}#{log['gender']}"]
        update_ids << log['id']
      end
    end

    # update logs
    unless update_ids.empty?
      query = "
      UPDATE eligibility_logs
      SET using_dummy_member = TRUE
      WHERE id IN (#{update_ids.join(',')})
    "
      if @dry_run
        puts query
      else
        conn.exec(query)
      end
    end

    # output result
    count = cap
    puts "backfilled #{count}/#{total}"
  end
end

